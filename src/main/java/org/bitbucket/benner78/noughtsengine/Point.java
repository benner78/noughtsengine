package org.bitbucket.benner78.noughtsengine;

/**
 * A simple point tuple.
 */
public class Point
{
    final int row;
    final int col;

    /**
     * Point constructor
     * @param _row row element
     * @param _col column element
     */
    public Point(int _row, int _col)
    {
        row = _row;
        col = _col;
    }
}
