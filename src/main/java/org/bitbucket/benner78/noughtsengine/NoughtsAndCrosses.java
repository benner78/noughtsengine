package org.bitbucket.benner78.noughtsengine;

import java.util.Scanner;

public class NoughtsAndCrosses
{
    public static void main(String[] args)
    {
        Game game = new Game(Player.CROSS, 4, 3);
        game.printGameState();

        Scanner scanner = new Scanner(System.in);
        while (game.getLegalMoveCount() > 0 && game.getWinner() == Player.NONE)
        {
            System.out.print("Enter move row,col: ");
            String pos = scanner.next();
            String[] parts = pos.split(",");
            int row = Integer.parseInt(parts[0]);
            int col = Integer.parseInt(parts[1]);

            System.out.printf("Playing (%d,%d)\n", row, col);
            game.play(new Point(row, col));
            game.printGameState();

            if (game.getWinner() != Player.NONE || game.getLegalMoveCount() == 0)
            {
                break;
            }

            UCT uct = new UCT(game);
            game = uct.pickMoveUCT();
            game.printGameState();
        }
    }
}
