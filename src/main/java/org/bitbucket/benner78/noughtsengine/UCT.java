package org.bitbucket.benner78.noughtsengine;

import java.util.Collections;
import java.util.Comparator;

/**
 * Implement Upper Confidence Bound 1 for Trees algorithm.
 */
public class UCT
{
    private final boolean DEBUG = false;

    /**
     * Construct UCT object
     * @param game The game object to choose next move for.
     */
    public UCT(Game game)
    {
        root = new TreeNode(game);
    }

    /**
     * Generate a move using UCT algorithm.
     */
    public Game pickMoveUCT()
    {
        final int maxUCTIterations = root.getGame().getSize() * 500;

        for (int i = 0; i < maxUCTIterations; ++i)
        {
            TreeNode node = root;

            // Descend based on UCT score.
            while (node.isFullyExpanded() && node.notLeaf())
            {
                node = node.getChildNodeUCT();
            }

            // If possible add new child node.
            if (!node.isFullyExpanded() && node.notLeaf())
            {
                node = node.addChild();
            }

            // Evaluate position with Monte Carlo playout.
            node.playout();
        }

        if (DEBUG)
        {
            for (TreeNode child : root.getChildren())
            {
                System.out.printf("Parent visits %d, Visits %d, score %f, uct score %1.10f\n",
                        root.getVisits(), child.getVisits(),
                        child.getScore(), child.getUCTScore());
            }
        }

        // Pick child node with most visits.
        TreeNode winner = Collections.max(root.getChildren(), new Comparator<TreeNode>() {
                    @Override
                    public int compare(TreeNode first, TreeNode second) {
                        if (first.getVisits() > second.getVisits())
                            return 1;
                        else if (first.getVisits() < second.getVisits())
                            return -1;
                        return 0;
                    }
                });

        if (DEBUG)
        {
            System.out.printf("Winner Visits %d, score %f, uct score %f\n", winner.getVisits(),
                    winner.getScore(), winner.getUCTScore());
        }

        return new Game(winner.getGame());
    }

    private TreeNode root;
}
