package org.bitbucket.benner78.noughtsengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
   A simple noughts and crosses board.
*/
class Board
{
    private int size;
    private int winLength;
    private int emptyCount;
    private Player[] board;

    /**
       Construct an empty noughts and crosses board.
    */
    Board(int size, int winLength)
    {
        this.size = size;
        this.winLength = winLength;
        emptyCount = size * size;
	    board = new Player[emptyCount];
	    Arrays.fill(board, 0, emptyCount, Player.NONE);
    }

    /**
     * Copy constructor
     * @param b board to copy.
     */
    Board(Board b)
    {
        size = b.getSize();
        winLength = b.winLength;
        emptyCount = b.getEmptyCount();
        board = new Player[size * size];
        System.arraycopy(b.board, 0, board, 0, size * size);
    }

    /**
       Play a nought or cross onto the board.
       @param player whether to play a nought or a cross
       @param point position to play
       @return whether move was successful
    */
    boolean play(Player player, Point point)
    {
        final int idx = point.row * size + point.col;

        if (board[idx] == Player.NONE)
        {
            board[idx] = player;
            emptyCount--;
            return true;
        }
        return false;
    }

    /**
     Play a random move for player.
     @param player whether to play a nought or a cross
     @return whether move was successful
     */
    boolean playRandom(Player player)
    {
        final Random rand = new Random();

        if (emptyCount > 0)
        {
            while (!play(player, new Point(rand.nextInt(size), rand.nextInt(size))));
            return true;
        }
        return false;
    }

    /**
       Check whether a player has won the game.
       @return the winning player or NONE
    */
    Player getWinner()
    {
        Player winner = Player.NONE;

        for (int row = 0; row < size; row++)
        {
            for (int col = 0; col < size; col++)
            {
                if (col <= size - winLength)
                {
                    winner = checkRow(row, col, winLength);
                }
                if (row <= size - winLength && winner == Player.NONE)
                {
                    winner = checkCol(row, col, winLength);
                }
                if ((row <= size - winLength) && (col <= size - winLength) && winner == Player.NONE)
                {
                    winner = checkDiag(row, col, winLength, 0);
                }
                if ((row <= size - winLength) && (col >= winLength - 1) && winner == Player.NONE)
                {
                    winner = checkDiag(row, col, winLength, 1);
                }
                if (winner != Player.NONE)
                {
                    return winner;
                }
            }
        }
        return winner;
    }

    /**
       Check for winLength matching symbols in row
       @param row row to start
       @param col column to start
       @param winLength number of symbols in a row
       @return winning player or NONE
    */
    private Player checkRow(int row, int col, int winLength)
    {
        final int startPos = row * size + col;
        final int stride = 1;
        return checkLine(startPos, stride, winLength);
    }

    /**
     Check for winLength matching symbols in column
     @param row row to start
     @param col column to start
     @param winLength number of symbols in a row
     @return winning player or NONE
     */
    private Player checkCol(int row, int col, int winLength)
    {
        final int startPos = row * size + col;
        final int stride = size;
        return checkLine(startPos, stride, winLength);
    }

    /**
     Check for winLength matching symbols on diagonal
     @param row row to start
     @param col column to start
     @param winLength number of symbols in a row
     @param idx 0 for down right, 1 for down left
     @return winning player or NONE
     */
    private Player checkDiag(int row, int col, int winLength, int idx)
    {
        final int startPos = row * size + col;
        final int stride = (idx == 0) ? size + 1 : size - 1;
        return checkLine(startPos, stride, winLength);
    }

    /**
       Generic check for winLength matching symbols
       @param startPos first board position to check
       @param stride step to next position to check
       @param winLength how many symbols in a row
       @return winning player or NONE
    */
    private Player checkLine(int startPos, int stride, int winLength)
    {
        for (int i = 0; i < winLength - 1; i++)
        {
            if (board[startPos + i * stride] != board[startPos + (i + 1) * stride])
            {
                return Player.NONE;
            }
        }
        return board[startPos];
    }

    /**
     Print board state to console.
     */
    void printBoard()
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                System.out.printf("%s", board[i * size + j]);
                if (j < size - 1) System.out.printf("|");
                else System.out.printf("\n");
            }
            if (i < size - 1)
            {
                for (int j = 0; j < 2 * size - 1; j++)
                {
                    System.out.printf("-");
                }
            }
            System.out.printf("\n");
        }
    }

    /**
     * Get number of empty points on the board.
     * @return empty count
     */
    int getEmptyCount()
    {
        return emptyCount;
    }

    /**
     * Get list of empty points on the board.
     * @return  list of empty points
     */
    ArrayList<Point> getEmptyPoints()
    {
        ArrayList<Point> emptyPoints = new ArrayList<>();

        for (int col = 0; col < size; col++)
        {
            for (int row = 0; row < size; row++)
            {
                if (board[row * size + col] == Player.NONE)
                {
                    emptyPoints.add(new Point(row, col));
                }
            }
        }
        return emptyPoints;
    }

    /**
     * Get position on board
     * @param point on the board
     * @return player or NONE
     */
    Player getPos(Point point)
    {
        return board[point.row * size + point.col];
    }

    /**
     * Get board size
     * @return board size
     */
    private int getSize()
    {
        return size;
    }
}
