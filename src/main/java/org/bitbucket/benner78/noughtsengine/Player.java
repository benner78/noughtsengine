package org.bitbucket.benner78.noughtsengine;

/**
   Noughts or crosses board position state.
*/
public enum Player
{
    NOUGHT("O"),
    CROSS("X"),
    NONE(" ");

    private String string;

    Player(String name){string = name;}

    @Override
    public String toString()
    {
	return string;
    }
}
