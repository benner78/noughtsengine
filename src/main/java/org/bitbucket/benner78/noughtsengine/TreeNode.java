package org.bitbucket.benner78.noughtsengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

class TreeNode
{
    /**
     * Control the trade-off between exploration and exploitation.
     */
    private final static float BIAS = 1.0f;

    /**
     * Create a root node with a new game.
     * @param _game A game to be at the root.
     */
    TreeNode(Game _game)
    {
        parent = null;
        game = new Game(_game);
        score = 0;
        visits = 0;
        leaf = false;
        initLegalMoves();
    }

    /**
     * Create new node in the game tree.
     * @param _parent pointer to the node's parent.
     */
    private TreeNode(TreeNode _parent)
    {
        parent = _parent;
        game = new Game(parent.game);
        score = 0;
        visits = 0;
        leaf = false;
        initLegalMoves();
    }

    /**
     * Add child node and play a turn in it's game state. If
     * this is the last possible turn mark it as a leaf node
     * and back propagate the result.
     */
    TreeNode addChild()
    {
        TreeNode child = new TreeNode(this);

        final Random rand = new Random();

        // Find a random empty point and remove from the list.
        int pointIdx = rand.nextInt(legalMoves.size());
        Point point = legalMoves.get(pointIdx);
        legalMoves.remove(pointIdx);

        // Play the move in child game.
        child.game.play(point);

        // Make sure child has the empty point list.
        child.initLegalMoves();

        Player winner = child.game.getWinner();

        if (child.game.getLegalMoveCount() == 0 || winner != Player.NONE)
        {
            backPropagate(winner);
            child.leaf = true;
        }

        children.add(child);

        return child;
    }

    /**
     * Perform a random playout and backpropogate result.
     */
    void playout()
    {
        Player winner = (new Game(game)).playout();
        backPropagate(winner);
    }

    /**
     * Increment the score of all parent nodes which have the
     * correct parity.
     * @param winner winner
     */
    private void backPropagate(Player winner)
    {
        incrementVisits();

        if ((winner != game.getTurn()) && (winner != Player.NONE))
        {
            score++;
        }
        else if (winner == Player.NONE)
        {
            score += 0.5;
        }

        if (parent != null)
        {
            parent.backPropagate(winner);
        }
    }

    /**
     * Get score of this node.
     * @return score
     */
    double getScore()
    {
        return score;
    }

    /**
     * Get number of times this node has been visited
     * @return number of visits.
     */
    int getVisits()
    {
        return visits;
    }

    /**
     * Increment this nodes visit counter.
     */
    private void incrementVisits()
    {
        visits++;
    }

    /**
     * Get the child which has the greatest UCT score.
     * @return child TreeNode.
     */
    TreeNode getChildNodeUCT()
    {
        return Collections.max(children, new Comparator<TreeNode>() {
            @Override
            public int compare(TreeNode first, TreeNode second) {
                if (first.getUCTScore() > second.getUCTScore())
                    return 1;
                else if (first.getUCTScore() < second.getUCTScore())
                    return -1;
                return 0;
            }
        });
    }

    /**
     * Calculate UCT score for this node.
     * @return UCT score
     */
    double getUCTScore()
    {
        int parentVisits = 1;

        if (parent != null)
        {
            parentVisits += parent.getVisits();
        }
        return score/visits + BIAS * Math.sqrt(Math.log(parentVisits)/visits);
    }

    /**
     * Get this nodes children
     * @return List of children
     */
    List<TreeNode> getChildren()
    {
        return children;
    }

    /**
     * Is this node fully expanded?
     * @return Whether all possible children exist.
     */
    boolean isFullyExpanded()
    {
        return legalMoves.size() == 0;
    }

    /**
     * Is this a non-terminal node.
     * @return not leaf
     */
    boolean notLeaf()
    {
        return !leaf;
    }

    /**
     * Set up this nodes's array of empty board positions.
     */
    private void initLegalMoves()
    {
        legalMoves = game.getLegalMoves();
    }

    /**
     * Get this node's Game object
     * @return Game
     */
    Game getGame()
    {
        return game;
    }

    private TreeNode parent;
    private ArrayList<TreeNode> children = new ArrayList<>();
    private ArrayList<Point> legalMoves;
    private Game game;
    private double score;
    private int visits;
    private boolean leaf;
}
