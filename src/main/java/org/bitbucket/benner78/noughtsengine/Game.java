package org.bitbucket.benner78.noughtsengine;

import java.util.ArrayList;

/**
 * Class to hold game state.
 */
public class Game
{
    private int size;
    private Board board;
    private Player turn;
    private static int gameId;
    /**
     * Construct game object.
     * @param firstTurn Who plays first.
     */
    public Game(Player firstTurn, int size, int winLength)
    {
        this.size = size;
        board = new Board(size, winLength);
        turn = firstTurn;
        gameId++;
    }

    /**
     * Copy constructor.
     * @param g Game to copy
     */

    public Game(Game g)
    {
        size = g.getSize();
        board = new Board(g.board);
        turn = g.turn;
        gameId = g.getGameId();
    }

    /**
     * Play a turn
     * @param point to play
     * @return success
     */
    public boolean play(Point point)
    {
        if (board.play(turn, point))
        {
            nextTurn();
            return true;
        }
        return false;
    }

    /**
     * Perform a random playout from current game state.
     * @return Winner, or NONE for a draw.
     */
    Player playout()
    {
        Player winner = getWinner();

        if (winner != Player.NONE)
        {
            return winner;
        }
        while(board.playRandom(turn))
        {
            nextTurn();
            winner = getWinner();
            if (winner != Player.NONE)
            {
                return winner;
            }
        }
        return winner;
    }

    /**
     * Print game state to console.
     */
    void printGameState()
    {
        board.printBoard();
        Player winner = getWinner();

        if (board.getEmptyCount() > 0 && winner == Player.NONE)
        {
            System.out.printf("%s to play.\n", turn);
        }
        else
        {
            if (winner != Player.NONE)
            {
                System.out.printf("%s wins!\n", winner);
            }
            else
            {
                System.out.println("Draw.\n");
            }
        }
    }

    /**
     * Get legal move count
     * @return The number of legal moves that are possible
     */
    public int getLegalMoveCount()
    {
        return board.getEmptyCount();
    }

    /**
     * Get list of all possible legal moves
     * @return List of moves.
     */
    ArrayList<Point> getLegalMoves()
    {
        return board.getEmptyPoints();
    }

    /**
     * Check for winner
     * @return Player who has won or NONE.
     */
    public Player getWinner()
    {
        return board.getWinner();
    }

    /**
     * @return next player to play.
     */
    public Player getTurn()
    {
        return turn;
    }

    /**
     * Get state of a point on the board
     * @param point the requested point
     * @return the player who occupies the point (or NONE)
     */
    public Player getPos(Point point)
    {
        return board.getPos(point);
    }

    /**
     *  Change game state to be next player's turn.
     */
    private void nextTurn()
    {
        turn = (turn == Player.CROSS) ? Player.NOUGHT : Player.CROSS;
    }

    /**
     * Get board size
     * @return size
     */
    public int getSize()
    {
        return size;
    }

    public int getGameId()
    {
        return gameId;
    }

}
